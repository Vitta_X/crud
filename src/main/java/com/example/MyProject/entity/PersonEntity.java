package com.example.MyProject.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@RequiredArgsConstructor
@Table(name = "persons", schema = "crud")
public class PersonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "person_seq")
    @SequenceGenerator(name = "person_seq",
            sequenceName = "person_id_seq", schema = "crud",
            allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "age")
    private Integer age;

    @Column(name = "gender")
    private Character gender;

    @Column(name = "date_creat", updatable = false)
    private LocalDateTime dateCreat;

    @Column(name = "date_update")
    private LocalDateTime dateUpdate;

    @PrePersist
    public void toCreat() {
        setDateCreat(LocalDateTime.now());
        setDateUpdate(getDateCreat());
    }
    @PreUpdate
    public void toUpdate(){
        setDateUpdate(LocalDateTime.now());
    }


    @OneToMany(fetch = FetchType.EAGER,cascade ={CascadeType.ALL})
    @JoinColumn(name = "person_id")
    private List<ContactEntity> contacts;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private AddressEntity address;

}
