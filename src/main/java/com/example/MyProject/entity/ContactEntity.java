package com.example.MyProject.entity;

import com.example.MyProject.model.Enum.EnumContact;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@RequiredArgsConstructor

@Table(name = "contacts", schema = "crud")
public class ContactEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @Column(name = "enum_сontact")
    @Enumerated(EnumType.STRING)
    private EnumContact enumContact;

    @Column(name = "value")
    private String value;

    @Column(name = "person_id")
    private Long personId;
}
