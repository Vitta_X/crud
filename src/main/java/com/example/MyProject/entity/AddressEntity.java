package com.example.MyProject.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@RequiredArgsConstructor
@Data
@Entity
@Table(name = "address", schema = "crud")
public class AddressEntity {

    @Id
    @Column(name = "id",updatable = false)
    @GeneratedValue()
    private Long id;


    @Column(name = "city")
    private String city;


    @Column(name = "street")
    private String street;


    @Column(name = "house")
    private String house;

//    @OneToOne(mappedBy = "address")
//    private PersonEntity personEntity;
}


