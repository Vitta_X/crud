package com.example.MyProject.controller;

import com.example.MyProject.model.ContactDto;
import com.example.MyProject.model.PersonDto;
import com.example.MyProject.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/contact")
public class ContactController {

    final ContactService service;

    @PostMapping
    public void saveContact(@RequestBody ContactDto dto) {
        service.save(dto);
    }

    @GetMapping("/{id}")
    public ContactDto getContactById(@PathVariable Long id){
        return service.getById(id);
    }


    @GetMapping("/all")
    public List<ContactDto> getContactsAll(){
        return service.getAll();
    }

    @PutMapping("/{id}")
    public void updateContact(@PathVariable Long id,
                             @RequestBody ContactDto dto){
        service.updateContact(id,dto);
    }

    @DeleteMapping("/{id}")
    public void deleteContact(@PathVariable Long id){
        service.deleteContact(id);
    }
}
