package com.example.MyProject.controller;

import com.example.MyProject.model.PersonDto;
import com.example.MyProject.model.PersonRequest;
import com.example.MyProject.service.PersonService;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor

@RequestMapping("/person")
public class PersonController {

    final PersonService service;



    @PostMapping
    public void savePerson(@RequestBody  PersonRequest dto) {
        service.save(dto);
    }

    @GetMapping("/{id}")
    public PersonDto getPersonById(@PathVariable Long id){
       return service.getById(id);
    }

    @GetMapping("/all")
    public List<PersonDto> getPersonAll(){
        return service.getAll();
    }

    @PutMapping("/{id}")
    public void updatePerson(@PathVariable Long id,
                             @RequestBody PersonDto dto){
        service.updatePerson(id,dto);
    }

    @DeleteMapping("/{id}")
    public void deletePerson(@PathVariable Long id){
        service.deletePerson(id);
    }

}
