package com.example.MyProject.controller;

import com.example.MyProject.model.AddressDto;
import com.example.MyProject.model.PersonDto;
import com.example.MyProject.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/address")
public class AddressController {

    final AddressService service;

    @PostMapping
    public void saveAddress(@RequestBody AddressDto dto) {
        service.save(dto);
    }

    @GetMapping("/{id}")
    public AddressDto getById(@PathVariable Long id) {
        return service.getById(id);
    }

    @GetMapping("/all")
    public List<AddressDto> getAll(){
        return service.getAllAddress();
    }

    @PutMapping("/{id}")
    public void updateAddress(@PathVariable Long id,
                             @RequestBody AddressDto dto){
        service.updateAddress(id,dto);
    }
    @DeleteMapping("/{id}")
    public void deleteAddress(@PathVariable Long id){
        service.deleteAddress(id);
    }
}
