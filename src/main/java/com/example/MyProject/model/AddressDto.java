package com.example.MyProject.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class AddressDto {

    private Long id;


    private String city;


    private String street;


    private String house;

}
