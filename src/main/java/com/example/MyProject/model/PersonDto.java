package com.example.MyProject.model;

import com.example.MyProject.entity.AddressEntity;
import com.example.MyProject.entity.ContactEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor

public class PersonDto {

    private Long id;
    private String name;
    private String surname;
    private Integer age;
    private Character gender;
    private List<ContactDto> contacts;
    private AddressDto address;

}
