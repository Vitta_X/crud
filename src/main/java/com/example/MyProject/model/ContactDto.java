package com.example.MyProject.model;

import com.example.MyProject.model.Enum.EnumContact;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor

public class ContactDto {

    private Long id;

    private String enumContact;

    private String value;

    private Long personId;

}
