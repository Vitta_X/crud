package com.example.MyProject.model;

import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Validated

public class PersonRequest {
    private Long id;
    @NotBlank
    private String name;
    @NotBlank
    private String surname;
    @NotNull
    private Integer age;
    @NotBlank
    private Character gender;
    @NotEmpty()
    private AddressDto address;
    @NotEmpty()
    private List<ContactDto> contacts;

}
