package com.example.MyProject.service;

import com.example.MyProject.entity.AddressEntity;
import com.example.MyProject.entity.PersonEntity;
import com.example.MyProject.exception.PersonNotFoundException;
import com.example.MyProject.model.AddressDto;
import com.example.MyProject.model.PersonDto;
import com.example.MyProject.model.PersonRequest;
import com.example.MyProject.repository.AddressRepository;
import com.example.MyProject.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service

@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository repository;
    private final AddressRepository addressRepository;
    private final ModelMapper mapper;

    public void save(PersonRequest dto) {
        PersonEntity entity = mapper.map(dto, PersonEntity.class);
        repository.save(entity);
//        var personEntity = repository.save(entity);
//        var addressEntity = mapper.map(dto.getAddress(), AddressEntity.class);
//        addressEntity.setPersonEntity(personEntity);
//        addressRepository.save(addressEntity);
    }

    public PersonDto getById(Long id) {
        PersonEntity entity = repository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException("Person by id:" + id + " not found"));
//        var personResponse = mapper.map(entity, PersonRequest.class);
//        var address = addressRepository.finByPersonEntityId(id);
//        personResponse.setAddress(mapper.map(address, AddressDto.class));
        return mapper.map(entity,PersonDto.class);
    }

    public List<PersonDto> getAll() {
        List<PersonEntity> list = repository.findAll();
        return list.stream().map(e -> mapper.map(e, PersonDto.class)).collect(Collectors.toList());
    }

    public void updatePerson(Long id, PersonDto dto) {
        PersonEntity entity = repository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException("Person by id:" + id + " not found"));
        mapper.map(dto, entity);
        repository.save(entity);
    }

    public void deletePerson(Long id) {
        PersonEntity entity = repository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException("Person by id:" + id + "not found"));
        repository.delete(entity);
    }
}
