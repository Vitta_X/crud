package com.example.MyProject.service;

import com.example.MyProject.entity.ContactEntity;
import com.example.MyProject.exception.ContactNotFoundException;
import com.example.MyProject.exception.PersonNotFoundException;
import com.example.MyProject.model.ContactDto;
import com.example.MyProject.model.PersonDto;
import com.example.MyProject.repository.ContactRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ContactService {

    final ModelMapper mapper;
    final ContactRepository repository;


    public void save(ContactDto dto) {
        ContactEntity entity = mapper.map(dto, ContactEntity.class);
        repository.save(entity);
    }

    public ContactDto getById(Long id) {
        ContactEntity entity = repository.findById(id)
                .orElseThrow(() -> new ContactNotFoundException("Contact by id:" + id + " not found"));
        return mapper.map(entity, ContactDto.class);
    }

    public List<ContactDto> getAll() {
        List<ContactEntity> list = repository.findAll();
        return list.stream().
                map(e-> mapper.map(e,ContactDto.class)).
                collect(Collectors.toList());
    }

    public void updateContact(Long id, ContactDto dto) {
       ContactEntity entity = repository.findById(id).orElseThrow(() -> new PersonNotFoundException("Contact by id:" + id + " not found"));
        mapper.map(dto,entity);
        repository.save(entity);
    }

    public void deleteContact(Long id) {
       ContactEntity entity = repository.findById(id).orElseThrow(() -> new ContactNotFoundException("Contact by id:" + id + " not found"));
        repository.delete(entity);
    }
}
