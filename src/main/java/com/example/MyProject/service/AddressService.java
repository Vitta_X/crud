package com.example.MyProject.service;

import com.example.MyProject.entity.AddressEntity;
import com.example.MyProject.entity.ContactEntity;
import com.example.MyProject.entity.PersonEntity;
import com.example.MyProject.exception.PersonNotFoundException;
import com.example.MyProject.model.AddressDto;
import com.example.MyProject.model.ContactDto;
import com.example.MyProject.repository.AddressRepository;
import com.example.MyProject.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.modelmapper.ModelMapper;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AddressService {

    final ModelMapper mapper;
    final AddressRepository repository;
    final PersonRepository personRepository;


    public void save(AddressDto dto) {
//        var personEntity = personRepository
//                .findById(dto.getPersonId())
//                .orElse(new PersonEntity());
        AddressEntity entity = mapper.map(dto, AddressEntity.class);
//        entity.setPersonEntity(personEntity);
        repository.save(entity);
    }

    public AddressDto getById(Long id) {
        Optional<AddressEntity> entity = repository.findById(id);
        return mapper.map(entity,AddressDto.class);
    }


    public List<AddressDto> getAllAddress() {
        List<AddressEntity> list = repository.findAll();
        return list.stream()
                .map(e -> mapper.map(e,AddressDto.class)).collect(Collectors.toList());
    }


    public void updateAddress(Long id, AddressDto dto) {
        AddressEntity entity = repository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException("Address by id:" + id + " not found"));
        mapper.map(dto, entity);
        repository.save(entity);
    }


    public void deleteAddress(Long id) {
        AddressEntity entity = repository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException("Address Person by id:" + id + "not found"));
        repository.delete(entity);
    }
}
