package com.example.MyProject.repository;

import com.example.MyProject.entity.AddressEntity;
import com.example.MyProject.entity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<AddressEntity, Long> {
    @Query(value = "SELECT * FROM address a WHERE a.person_entity_id = :id ",
            nativeQuery = true)
    AddressEntity finByPersonEntityId(@Param("id") Long id);
}

