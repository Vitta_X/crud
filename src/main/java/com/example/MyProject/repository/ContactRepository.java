package com.example.MyProject.repository;

import com.example.MyProject.entity.AddressEntity;
import com.example.MyProject.entity.ContactEntity;
import com.example.MyProject.model.Enum.EnumContact;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<ContactEntity,Long> {





}
